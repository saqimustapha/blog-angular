# Blog-Angular

L'objectif de ce projet est de créer un blog en utilisant Spring-Boot pour le backend et Angular pour le 
frontend.

## Fonctionnalités :

- Consulter la liste des articles
- Créer des articles
- Consulter un article spécifique
- Modifier ou supprimer un article

## Travail réalisé :

  1. Création des différentes pages du blog (maquettes) : 
   
  - [Accueil](images/MyBlog%20.png) : liste des articles
  - [Ajouter un article](images/MyBlog1.png)
  - [Modifier un article](images/MyBlog2.png)
  
  2. Création d'une base de données (Table des articles avec JDBC et Repository Article)
  3. [Création d'une API Rest avec Spring-Boot (Contrôleur Article)](https://gitlab.com/saqimustapha/blog-sprin) 
  4. Création du front du Blog avec Angular
  5. Requêter l'API Rest depuis les services de Angular
  
















