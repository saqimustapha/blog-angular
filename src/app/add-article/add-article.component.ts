import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ArticleService } from '../article.service';
import { Article } from '../entity';

@Component({
  selector: 'app-add-article',
  templateUrl: './add-article.component.html',
  styleUrls: ['./add-article.component.css']
})
export class AddArticleComponent implements OnInit {
  article: Article = {
    title: '',
    content: ''
  };
  
  constructor(private as:ArticleService, private router: Router) { }

  ngOnInit(): void {
  }
  
  addArticle() {
    this.as.add(this.article).subscribe(() => {
      this.router.navigate(['/']);
    });
  }

}
