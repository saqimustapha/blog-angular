import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddArticleComponent } from './add-article/add-article.component';
import { HomeComponent } from './home/home.component';


import { SingleArticleComponent } from './single-article/single-article.component';


const routes: Routes = [
  {path: '', component : HomeComponent},
  {path: 'add-article', component : AddArticleComponent},
  {path: 'article/:id', component : SingleArticleComponent},
 
  
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

 