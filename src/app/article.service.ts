import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Article } from './entity';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {

  constructor(private http:HttpClient) { }

  getAll() {
    return this.http.get<Article[]>(environment.apiUrl+'/api/article');
  }

  getById(id:number){
    return this.http.get<Article>(environment.apiUrl+'/api/article/'+id);
  }

  add(article:Article){
    return this.http.post<Article>(environment.apiUrl+'/api/article', article);
  }

  delete(id:number) {
    return this.http.delete(environment.apiUrl+'/api/article/'+id);
  }

  put(article:Article){
    return this.http.put<Article>(environment.apiUrl+'/api/article/'+article.id, article);
  }

}
