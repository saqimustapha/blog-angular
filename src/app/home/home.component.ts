import { Component, OnInit } from '@angular/core';
import { Article } from '../entity';
import { ArticleService } from '../article.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  articles: Article[] = [];
  displayEditForm: boolean = false;
  modifiedAr: Article = {
    title: "", content: "" };

  constructor(private as: ArticleService) { }

  ngOnInit(): void {
    this.as.getAll().subscribe(data => this.articles = data);
  }

  delete(id: number) {
    this.as.delete(id).subscribe();
    this.removeFromList(id);
  }
  removeFromList(id: number) {
    this.articles.forEach((article, article_index) => {
      if (article.id == id) {
        this.articles.splice(article_index, 1);
      }
    });
  }

  showEditForm(article: Article) {
    this.displayEditForm = true;
    this.modifiedAr = Object.assign({}, article);
  }

  update(article: Article) {
    this.as.put(article).subscribe();
    this.updateArticleInList(article);
    this.displayEditForm = false;
  }

  updateArticleInList(article: Article) {
    this.articles.forEach((ar_list) => {
      if (ar_list.id == article.id) {
        ar_list.title = article.title;
        ar_list.content = article.content;
        ar_list.date = article.date;
      }
    });
  }

}
